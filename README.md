
##Installation

Require this package in your `composer.json` and run `composer update`.

```php
"flowcontrol/laravel-sidebar": "~2.1"
```

After updating composer, add the ServiceProvider to the providers array in `config/app.php`

```php
'FlowControl\Sidebar\SidebarServiceProvider',
```

Add the package middleware to `App\Http\Kernel`:

```php
`'FlowControl\Sidebar\Middleware\ResolveSidebars'`
```

To publish the default views use:

```php
php artisan vendor:publish --tag="views"
```

To publish the config use:

```php
php artisan vendor:publish --tag="config"
```

##Documentation

This Sidebar is baed on: Maatwebsite/Laravel-Sidebar
See the wiki: https://github.com/Maatwebsite/Laravel-Sidebar/wiki