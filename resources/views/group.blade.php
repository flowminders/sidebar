@if($group->shouldShowHeading())
    <li id="{{ $group->getID() }}" class="menu-title">{{ $group->getName() }}</li>
@endif

@foreach($items as $item)
    {!! $item !!}
@endforeach
