<?php

namespace FlowControl\Sidebar;

use Serializable;

interface ShouldCache extends Serializable
{
    /**
     * @return array
     */
    public function getCacheables();
}
