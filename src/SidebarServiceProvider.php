<?php

namespace FlowControl\Sidebar;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use FlowControl\Sidebar\Infrastructure\SidebarFlusherFactory;
use FlowControl\Sidebar\Infrastructure\SidebarResolverFactory;

class SidebarServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = false;

    /**
     * @var string
     */
    protected $shortName = 'sidebar';

    /**
     * Boot the service provider.
     * @return void
     */
    public function boot()
    {
        $this->registerViews();
    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        // Register config
        $this->registerConfig();

        // Bind SidebarResolver
        $this->app->bind('FlowControl\Sidebar\Infrastructure\SidebarResolver', function (Application $app) {

            $resolver = SidebarResolverFactory::getClassName(
                $app['config']->get('sidebar.cache.method')
            );

            return $app->make($resolver);
        });

        // Bind SidebarFlusher
        $this->app->bind('FlowControl\Sidebar\Infrastructure\SidebarFlusher', function (Application $app) {

            $resolver = SidebarFlusherFactory::getClassName(
                $app['config']->get('sidebar.cache.method')
            );

            return $app->make($resolver);
        });

        // Bind manager
        $this->app->singleton('FlowControl\Sidebar\SidebarManager');

        // Bind Menu
        $this->app->bind(
            'FlowControl\Sidebar\Menu',
            'FlowControl\Sidebar\Domain\DefaultMenu'
        );

        // Bind Group
        $this->app->bind(
            'FlowControl\Sidebar\Group',
            'FlowControl\Sidebar\Domain\DefaultGroup'
        );

        // Bind Item
        $this->app->bind(
            'FlowControl\Sidebar\Item',
            'FlowControl\Sidebar\Domain\DefaultItem'
        );

        // Bind Badge
        $this->app->bind(
            'FlowControl\Sidebar\Badge',
            'FlowControl\Sidebar\Domain\DefaultBadge'
        );

        // Bind Append
        $this->app->bind(
            'FlowControl\Sidebar\Append',
            'FlowControl\Sidebar\Domain\DefaultAppend'
        );

        // Bind Renderer
        $this->app->bind(
            'FlowControl\Sidebar\Presentation\SidebarRenderer',
            'FlowControl\Sidebar\Presentation\Illuminate\IlluminateSidebarRenderer'
        );
    }

    /**
     * Register views.
     * @return void
     */
    protected function registerViews()
    {
        $location = __DIR__ . '/../resources/views';

        $this->loadViewsFrom($location, $this->shortName);

        $this->publishes([
            $location => base_path('resources/views/vendor/' . $this->shortName),
        ], 'views');
    }

    /**
     * Register config
     * @return void
     */
    protected function registerConfig()
    {
        $location = __DIR__ . '/../config/' . $this->shortName . '.php';

        $this->mergeConfigFrom(
            $location, $this->shortName
        );

        $this->publishes([
            $location => config_path($this->shortName . '.php'),
        ], 'config');
    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return [
            'FlowControl\Sidebar\Menu',
            'FlowControl\Sidebar\Item',
            'FlowControl\Sidebar\Group',
            'FlowControl\Sidebar\Badge',
            'FlowControl\Sidebar\Append',
            'FlowControl\Sidebar\SidebarManager',
            'FlowControl\Sidebar\Presentation\SidebarRenderer',
            'FlowControl\Sidebar\Infrastructure\SidebarResolver'
        ];
    }
}
