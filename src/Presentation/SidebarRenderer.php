<?php

namespace FlowControl\Sidebar\Presentation;

use FlowControl\Sidebar\Sidebar;

interface SidebarRenderer
{
    /**
     * @param Sidebar $sidebar
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render(Sidebar $sidebar);
}
