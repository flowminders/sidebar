<?php

namespace FlowControl\Sidebar\Exceptions;

class SidebarResolverNotSupported extends \Exception
{
}
