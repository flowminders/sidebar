<?php

namespace FlowControl\Sidebar\Exceptions;

class SidebarFlusherNotSupported extends \Exception
{
}
