<?php

namespace FlowControl\Sidebar\Infrastructure;

interface SidebarFlusher
{
    /**
     * Flush
     *
     * @param $name
     */
    public function flush($name);
}
