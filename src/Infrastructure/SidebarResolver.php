<?php

namespace FlowControl\Sidebar\Infrastructure;

use FlowControl\Sidebar\Sidebar;

interface SidebarResolver
{
    /**
     * @param string $name
     *
     * @return Sidebar
     */
    public function resolve($name);
}
